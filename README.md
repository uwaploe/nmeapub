# NMEA Publisher

This application reads one or more NMEA data sources from serial ports or TCP/IP ports and publishes the sentences to a [NATS Streaming Server](https://nats.io/documentation/streaming/nats-streaming-intro/).

## Configuration File

The single command line argument specifies a [TOML](https://github.com/toml-lang/toml) format configuration file. The example below list three NMEA sources, two via serial ports and one via a TCP/IP serial server. The *baud* setting defaults to 4800 for serial-port sources and is ignored for network sources.

``` toml
[nats]
url = "nats://localhost:4222"
cluster = "test-cluster"
[[source]]
name = "/dev/ttyUSB0"
tag = "VESSEL"
baud = 4800
[[source]]
name = "/dev/ttyUSB1"
tag = "FOCUS"
baud = 38400
[[source]]
name = "192.168.1.10:4001"
tag = "SURVEY"
```

## Publishing

Each sentence is published as a separate message to two subjects (channels).

The first subject name is based on the last three characters of the sentence type, so `$GPGGA` sentences will be published to *nmea.gga*. Messages published to this channel will contain the NMEA sentence preceded by the *tag* associated with the source, e.g.

```
SURVEY:$GPHDT,274.345,T*36
```

The second subject name is based on the *tag*. Messages on this channel will consist of just the NMEA sentence. The sentence above will also be published to *nmea.survey* (without the `SURVEY:` included).
