module bitbucket.org/uwaploe/nmeapub

go 1.15

require (
	bitbucket.org/mfkenney/go-nmea v1.5.2
	github.com/BurntSushi/toml v0.3.1
	github.com/armon/go-metrics v0.0.0-20180917152333-f0300d1749da // indirect
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/hashicorp/go-immutable-radix v1.0.0 // indirect
	github.com/hashicorp/go-msgpack v0.5.3 // indirect
	github.com/hashicorp/raft v1.0.0 // indirect
	github.com/lib/pq v1.0.0 // indirect
	github.com/nats-io/gnatsd v1.4.1 // indirect
	github.com/nats-io/go-nats v1.7.2 // indirect
	github.com/nats-io/go-nats-streaming v0.4.0
	github.com/nats-io/nats-streaming-server v0.12.0 // indirect
	github.com/nats-io/nkeys v0.0.2 // indirect
	github.com/nats-io/nuid v1.0.0 // indirect
	github.com/pascaldekloe/goe v0.0.0-20180627143212-57f6aae5913c // indirect
	github.com/prometheus/procfs v0.0.0-20190219184716-e4d4a2206da0 // indirect
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	go.etcd.io/bbolt v1.3.2 // indirect
	golang.org/x/crypto v0.0.0-20190219172222-a4c6cb3142f2 // indirect
	golang.org/x/sys v0.0.0-20190222072716-a9d3bda3a223 // indirect
	google.golang.org/appengine v1.4.0 // indirect
)
