// Nmeapub reads NMEA sentences from one or more sources and publishes
// them to a NATS Streaming Server.
package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"sync"
	"syscall"
	"time"

	nmea "bitbucket.org/mfkenney/go-nmea"
	"github.com/BurntSushi/toml"
	stan "github.com/nats-io/go-nats-streaming"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: nmeapub [options] configfile

Reads NMEA sentences from one or more sources and publishes
them to a NATS Streaming Server.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
)

type dataDesc struct {
	Name string    `toml:"name"`
	Tag  string    `toml:"tag"`
	Baud int       `toml:"baud"`
	rdr  io.Reader `toml:"-"`
}

type natsConfig struct {
	Url     string `toml:"url"`
	Cluster string `toml:"cluster"`
}

type sysConfig struct {
	Nats   natsConfig `toml:"nats"`
	Source []dataDesc `toml:"source"`
}

func publishSentence(sc stan.Conn, tag string, s nmea.Sentence) error {
	err := sc.Publish("nmea."+strings.ToLower(s.Id[2:]),
		[]byte(tag+":"+s.String()))
	if err != nil {
		return err
	}
	return sc.Publish("nmea."+strings.ToLower(tag), []byte(s.String()))
}

func openPorts(sources []dataDesc) []Port {
	var (
		p   Port
		err error
	)

	ports := make([]Port, 0)
	for _, d := range sources {
		if strings.Contains(d.Name, ":") {
			p, err = NetworkPort(d.Name, time.Second*3)
		} else {
			if d.Baud == 0 {
				d.Baud = 4800
			}
			p, err = SerialPort(d.Name, d.Baud, time.Second*3)
		}

		if err != nil {
			log.Fatalf("Cannot open port %q: %v", d.Name, err)
		}
		ports = append(ports, p)
	}

	return ports
}

func readSource(ctx context.Context, d dataDesc, sc stan.Conn) error {
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		s, err := nmea.ReadSentence(d.rdr)
		if err != nil {
			if !errors.Is(err, os.ErrDeadlineExceeded) {
				log.Printf("NMEA read error: %v", err)
			}
			continue
		}
		publishSentence(sc, d.Tag, s)
	}

}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	var cfg sysConfig
	b, err := ioutil.ReadFile(args[0])
	if err != nil {
		log.Fatalf("Cannot read config file: %v", err)
	}

	if err = toml.Unmarshal(b, &cfg); err != nil {
		log.Fatalf("Cannot parse configuration file: %v", err)
	}

	if len(cfg.Source) == 0 {
		log.Fatal("No NMEA sources specified, aborting")
	}

	sc, err := stan.Connect(cfg.Nats.Cluster, "nmea-pub", stan.NatsURL(cfg.Nats.Url))
	if err != nil {
		log.Fatalf("Cannot connect: %v", err)
	}
	defer sc.Close()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
		}
	}()

	log.Printf("NMEA Publisher starting: %s", Version)

	var wg sync.WaitGroup
	ports := openPorts(cfg.Source)
	for i, d := range cfg.Source {
		d.rdr = ports[i]
		wg.Add(1)
		go func(d dataDesc) {
			defer wg.Done()
			err := readSource(ctx, d, sc)
			if err != nil {
				log.Printf("Error on port %s: %v", d.Name, err)
			}
		}(d)
	}

	wg.Wait()
}
